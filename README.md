# CCats - Covert Channels are Tricky Stuff

CCats is a covert channel tool which works as man in the middle.
Our motivation is to create a tool which allows you to send files over a covert channel between two controlled servers.
<br/><br/>
This repository consists of three programs: a [server (daemon)](daemon/README.md), a [CLI-client](cli/README.md) and a [GUI-client](gui/README.md).
<br/><br/>
The server manages the uploaded files and transmits the files over the covert channel.
The clients let you connect and login to the server and upload files and start file transmission over the covert channel.

## The basic setup
```
   A           -----------------                        ------------------           B
   o  traffic  |               |                        |                |  traffic  o
  /|\ ---------| active Server | <== covert channel ==> | passive Server |--------- /|\
   \\          |               |                        |                |          //
               -----------------                        ------------------
                      ||                                       ||
               client connection                        client connection
                      ||                                       ||
               E  ----------                               ----------  F
               o  |        |                               |        |  o
              /|\ | Client |                               | Client | /|\
               \\ |        |                               |        | //
                  ----------                               ----------
```
E wants to send a file to F. A and E live together and B and F live together. E and F know that A will surf on B's website a lot so they decided to setup a covert channel which utilises the traffic of A and B as man in the middle. <br/>
A surfs on the website of B on Port p and produces traffic between them. E and F set up the two servers. The active server is the server which will be used to send a file to the passive server. <br/>
E sets the target IP on the active server side to the IP of A and F the target IP on the passive side to the IP of B. The target Port on both sides is set to p.
Now the servers are forwarding A's and B's traffic and can add secret data to the traffic. <br/>
E connects to the active server with his/her client and queues a file for sending over the covert channel. The file transmission will start once A surfs on B's website again. Now E and F have to wait until A produced enough traffic to transmit the file completely.
After the file transmission finished F can downlod the file with his/her client.

## Encryption
E and F can decide to use a common key for symmetrical encryption. Files will be encrypted on the client side so the servers cannot read the files as plain data. The covert channel does not add another encryption-layer so it's encouraged to use this feature.

## Dependencies
- libtins >=4.2
- Jsoncpp
- Boost >=1.70
- GNU Readline
- OpenSSL
- Qt5 (Core, Quick, Widgets)

### Dependencies for testing
- GTest
- GMock

### Installing dependencies on Ubuntu
Packages to install after minimal Ubuntu 19.10 installation:
```
sudo apt install qtdeclarative5-dev libboost-all-dev libjsoncpp-dev libreadline-dev libpcap-dev build-essential qml-module-qt-labs-platform libssl-dev
```

(NOTE: Due to a bug in jsoncpp, in the file size field of the GUI, all numbers after the decimal point are displayed as 0. This bug occurs in version 1.7.4 and is fixed in 1.9.1 (or possibly earlier).)

#### Install libtins
```
mkdir libtins
cd libtins
git clone https://github.com/mfontanini/libtins.git
sudo apt-get install libpcap-dev libssl-dev cmake
cd libtins
mkdir build
cd build
cmake ../ -DLIBTINS_ENABLE_CXX11=1
make
sudo make install
sudo ldconfig
```

#### Install boost
```
mkdir boost
cd boost
wget https://dl.bintray.com/boostorg/release/1.72.0/source/boost_1_72_0.tar.gz
tar -xf boost_1_72_0.tar.gz
cd boost_1_72_0
./bootstrap.sh
./b2
sudo ./b2 install
```

#### Build and install googletest
```
git clone https://github.com/google/googletest.git
cd googletest
mkdir build
cd build
cmake ..
make
make install
cd ../..
rm -rf googletest
```

## Build
CMake is used as build system. It should check which dependency is missing and tell you.

All three compontents can be built together using the top-level CMake file.
```
mkdir build
cd build
cmake ..
make
```
The components can also be built seperately following the same instructions from the `./daemon/`, `./cli/` and `./gui/` directory.

### Build with tests
```
mkdir build
cd build
cmake .. -DENABLE_TESTS=true
make
```
