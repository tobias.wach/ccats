#ifndef FILEMANAGERMOCK_H
#define FILEMANAGERMOCK_H

#include "../include/FileManager.h"
#include <gmock/gmock.h>

/**
 * @class FileManagerMock
 *
 * Gmock stub class for FileManager so you can test without writing and reading actual files.
 */
class FileManagerMock : public FileManager {
public:
	MOCK_METHOD((std::pair<bool, int>), openGetFile, (const std::string &filename), (override));
	MOCK_METHOD(bool, openPutFile, (const std::string &filename), (override));

	MOCK_METHOD(bool, isDownloading, (), (override));
	MOCK_METHOD(bool, isUploading, (), (override));

	MOCK_METHOD(void, cancelPut, (), (override));

	MOCK_METHOD(std::string, getGetBaseFileName, (), (override));
	MOCK_METHOD(std::string, getPutBaseFileName, (), (override));

	MOCK_METHOD(void, writePut, (const std::vector<char> &data), (override));
	MOCK_METHOD(std::vector<char>, readGet, (), (override));

	MOCK_METHOD(int, openList, (), (override));
	MOCK_METHOD(int, getRemainingListChunks, (), (override));
	MOCK_METHOD(int, getListSize, (), (override));
	MOCK_METHOD(std::vector<std::string>, getNextChunkFromList, (), (override));

	MOCK_METHOD(FileManager::Error, deleteFile, (const std::string &filename), (override));
	MOCK_METHOD((std::pair<std::vector<char>, FileManager::Error>), getBytesFromFile, (const std::string &filename, int numOfBytes), (override));

	MOCK_METHOD(int, openExtendedList, (), (override));
	MOCK_METHOD(int, getRemainingExtendedListChunks, (), (override));
	MOCK_METHOD(int, getExtendedListSize, (), (override));
	MOCK_METHOD((std::vector<std::tuple<std::string, std::string, double>>), getNextChunkFromExtendedList, (), (override));
	MOCK_METHOD(void, cancelExtendedList, (), (override));
};

#endif
