#ifndef CHANNELCONTROLSMOCK_H
#define CHANNELCONTROLSMOCK_H

#include "../include/CovertChannel/ChannelControls.h"
#include <gmock/gmock.h>

/**
 * @class ChannelControlsMock
 *
 * Gmock stub class for ChannelControls so you can test without starting an actual covert channel.
 */
class ChannelControlsMock : public ChannelControls {
public:
	MOCK_METHOD(bool, sendFile, (const std::string &fileName), (override));
	MOCK_METHOD((std::pair<uint32_t, uint32_t>), getProgress, (), (override));
	MOCK_METHOD(std::time_t, getTransferStart, (), (override));
	MOCK_METHOD(bool, isTransferRunning, (), (override));
	MOCK_METHOD(void, reset, (), (override));
	MOCK_METHOD(std::string, getFileName, (), (override));
};

#endif
