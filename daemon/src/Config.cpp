#include "../include/Config.h"

namespace Config {
std::map<std::string, std::string> storage;
};

bool Config::init(const std::string &filename) {
	std::ifstream ifile(filename);
	std::string line;

	if (ifile.is_open()) {
		while (getline(ifile, line)) {
			std::stringstream ss(line);
			std::string segment;
			std::vector<std::string> v;
			while (getline(ss, segment, '=')) {
				v.push_back(segment);
			}
			if (v.size() != 2) {
				return false;
			}
			storage.insert(std::pair<std::string, std::string>(v.at(0), v.at(1)));
		}
		return true;
	}
	return false;
}

std::string Config::getValue(const std::string &key) {
	auto it = storage.find(key);
	if (it != storage.end()) {
		return it->second;
	}

	return "";
}
