#include "../../include/CovertChannel/CovertChannel.h"
#include <cstdlib>
#include <iostream>

CovertChannel::CovertChannel(const std::string &innerInterface, const std::string &outerInterface, const std::string &innerForwardFilter,
                             const std::string &outerForwardFilter, const std::string &innerChannelFilter, const std::string &outerChannelFilter)
    : innerSender(innerInterface), outerSender(outerInterface) {
	Tins::SnifferConfiguration config;
	config.set_promisc_mode(true);
	config.set_immediate_mode(true);
	config.set_direction(PCAP_D_IN);
	config.set_snap_len(1500);

	try {
		innerForwardSniffer = new Tins::Sniffer(innerInterface, config);
		outerForwardSniffer = new Tins::Sniffer(outerInterface, config);
		innerChannelSniffer = new Tins::Sniffer(innerInterface, config);
		outerChannelSniffer = new Tins::Sniffer(outerInterface, config);

		setFilter(innerForwardFilter, outerForwardFilter, innerChannelFilter, outerChannelFilter);
	} catch (const Tins::pcap_error &e) {
		std::cerr << "An error accured setting up the sniffer: " << e.what() << std::endl;
		std::exit(EXIT_FAILURE);
	}
}

CovertChannel::~CovertChannel() {
	innerForwardSniffer->stop_sniff();
	outerForwardSniffer->stop_sniff();
	innerChannelSniffer->stop_sniff();
	outerChannelSniffer->stop_sniff();
	innerForwardSnifferThread.join();
	outerForwardSnifferThread.join();
	innerChannelSnifferThread.join();
	outerChannelSnifferThread.join();
	delete (innerForwardSniffer);
	delete (outerForwardSniffer);
	delete (innerChannelSniffer);
	delete (outerChannelSniffer);
}

void CovertChannel::setFilter(const std::string &innerForwardFilter, const std::string &outerForwardFilter, const std::string &innerChannelFilter,
                              const std::string &outerChannelFilter) {
	innerForwardSniffer->set_filter(innerForwardFilter);
	outerForwardSniffer->set_filter(outerForwardFilter);
	innerChannelSniffer->set_filter(innerChannelFilter);
	outerChannelSniffer->set_filter(outerChannelFilter);
}

void CovertChannel::startSniffing() {
	innerForwardSnifferThread = std::thread(&CovertChannel::startInnerForwardSniffing, this);
	outerForwardSnifferThread = std::thread(&CovertChannel::startOuterForwardSniffing, this);
	innerChannelSnifferThread = std::thread(&CovertChannel::startInnerChannelSniffing, this);
	outerChannelSnifferThread = std::thread(&CovertChannel::startOuterChannelSniffing, this);
}

void CovertChannel::startInnerForwardSniffing() { innerForwardSniffer->sniff_loop(make_sniffer_handler(this, &CovertChannel::handleForwardFromInner)); }

void CovertChannel::startOuterForwardSniffing() { outerForwardSniffer->sniff_loop(make_sniffer_handler(this, &CovertChannel::handleForwardFromOuter)); }

void CovertChannel::startInnerChannelSniffing() { innerChannelSniffer->sniff_loop(make_sniffer_handler(this, &CovertChannel::handleChannelFromInner)); }

void CovertChannel::startOuterChannelSniffing() { outerChannelSniffer->sniff_loop(make_sniffer_handler(this, &CovertChannel::handleChannelFromOuter)); }

bool CovertChannel::handleForwardFromOuter(Tins::PDU &pdu) {
	innerSender.send(pdu);

	return true;
}

bool CovertChannel::handleForwardFromInner(Tins::PDU &pdu) {
	outerSender.send(pdu);

	return true;
}
