#include "../../include/CovertChannel/ForwardChannel.h"

ForwardChannel::ForwardChannel(const std::string &innerInterface, const std::string &outerInterface) : CovertChannel(innerInterface, outerInterface) {}

ForwardChannel::~ForwardChannel() {}

bool ForwardChannel::handleChannelFromOuter(Tins::PDU &pdu) { return false; }

bool ForwardChannel::handleChannelFromInner(Tins::PDU &pdu) { return false; }

/* ChannelControls */

bool ForwardChannel::sendFile(const std::string &fileName) { return false; }

std::pair<uint32_t, uint32_t> ForwardChannel::getProgress() { return std::pair<uint32_t, uint32_t>(0, 0); }

bool ForwardChannel::isTransferRunning() { return false; }

void ForwardChannel::reset() {}

std::time_t ForwardChannel::getTransferStart() { return 0; }

std::string ForwardChannel::getFileName() { return ""; }

/* =============== */
