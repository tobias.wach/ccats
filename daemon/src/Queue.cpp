#include "../include/Queue.h"
#include "../include/Notifications.h"
#include <algorithm>

namespace Queue {
std::deque<std::string> queue;
std::mutex mtx;
ChannelControls *channel;
} // namespace Queue

bool Queue::push(const std::string &file) {
	// Don't queue if no channel is available
	if (channel == nullptr) {
		return false;
	}

	mtx.lock();
	queue.push_back(file);
	mtx.unlock();

	Notifications::newNotification("File \"" + file + "\" queued.");

	if (curTransfer().compare("") == 0) {
		schedule();
	}

	return true;
}

bool Queue::remove(const std::string &file) {
	if (curTransfer().compare(file) == 0) {
		// Reset channel and remove from queue
		channel->reset();
		Notifications::newNotification("Transfer of file \"" + file + "\" was aborted.");
		return true;
	} else {
		mtx.lock();
		auto it = std::find(queue.begin(), queue.end(), file);
		if (it == queue.end()) {
			return false;
		}
		queue.erase(it);
		mtx.unlock();
		return true;
	}
}

void Queue::schedule() {
	if (!queue.empty()) {
		mtx.lock();
		std::string file = queue.front();
		queue.pop_front();

		// success is false if file could not be sent or found
		bool success = channel->sendFile(file);
		mtx.unlock();

		if (!success) {
			Notifications::newNotification("File \"" + file + "\" could not be sent.");
			schedule();
		}
		// don't wait until transer finished because schedule must be called again
	} else {
		mtx.lock();
		mtx.unlock();
	}
}

std::string Queue::curTransfer() {
	if (channel == nullptr || !channel->isTransferRunning()) {
		return "";
	}

	return channel->getFileName();
}
