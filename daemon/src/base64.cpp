#include "../include/base64.h"

#include <boost/beast.hpp>

template <> std::string base64::decode<std::string>(const std::string &val) {
	std::string ret;
	ret.resize(boost::beast::detail::base64::decoded_size(val.size()));

	std::size_t read = boost::beast::detail::base64::decode(&ret.front(), val.c_str(), val.size()).first;

	ret.resize(read);
	return ret;
}

template <> std::vector<char> base64::decode<std::vector<char>>(const std::string &val) {
	std::vector<char> ret;
	ret.resize(boost::beast::detail::base64::decoded_size(val.size()));

	std::size_t read = boost::beast::detail::base64::decode(ret.data(), val.c_str(), val.size()).first;

	ret.resize(read);
	return ret;
}

template <> std::string base64::encode<std::string>(const std::string &val) {
	std::string ret;
	ret.resize(boost::beast::detail::base64::encoded_size(val.size()));

	std::size_t read = boost::beast::detail::base64::encode(&ret.front(), val.data(), val.size());

	ret.resize(read);
	return ret;
}

template <> std::string base64::encode<std::vector<char>>(const std::vector<char> &val) {
	std::string ret;
	ret.resize(boost::beast::detail::base64::encoded_size(val.size()));

	std::size_t read = boost::beast::detail::base64::encode(&ret.front(), val.data(), val.size());

	ret.resize(read);
	return ret;
}
