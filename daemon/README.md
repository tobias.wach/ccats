# Daemon

## Build
```
mkdir build
cd build
cmake ..
make
```

### Tests
To build tests just set the `ENABLE_TESTS` variable to true and rebuild the program.
```
mkdir build
cd build
cmake .. -DENABLE_TESTS=true
make
```

## Run
You must [create a config.txt](#daemon-configuration) before running the server.
```
bin/ccats
```

### Tests
```
make test
```

## Installation
You can adjust the installation directory as you like but don't forget to adjust the `ExecStart` option in the service file. The location of the configuration file is the first parameter for the daemon so you can change it as well.
Don't forget to create the files folder!!!

After building the program copy executable:
```
sudo cp bin/ccats /usr/bin
```

Copy service file:
```
sudo cp ccats.service /etc/systemd/system/
```

Enable and start service with:
```
sudo systemctl enable ccats.service
```
and start it
```
sudo systemctl start ccats.service
```

## Daemon configuration

The daemon is configurable by config.txt. You can use another config name using the config name as first parameter when launching the server.
The config file must be in the same directory from where you run the binary. If the file does not exist, the server will exit immediately.

### General Configuration Values
`port`: The port where the server listens for clients. Must be a valid port<br/>
`userdatabase`: The file where userdata is stored in format: user;password<br/>
`deleteAllowed`: Says if a client is allowed to delete files from its file directory<br/>
`filedirectory`: The directory where files from the clients will be stored and read from<br/>
`SSLenabled`: When set to true, the server will only use and accept SSL connections from clients. Set to false to disable this<br/>
`SSLcertificate`: The certificate file to use for SSL connections<br/>
`SSLprivatekey`: The private key file to use for SSL connections<br/>
`SSLdhparams`: The diffie-hellman file to use for SSL connections<br/>

#### Notes about SSL
To use SSL, certificates, keys and diffie-hellman parameters are required. To generate these, a convenience script `createsslfiles.sh` is provided.
The names of the output files are controlled with variables at the top of the script, modify these if desired.
Assuming default names, place the `user.crt`, `user.key` and `dh2048.pem` files somewhere convenient and configure the server accordingly.
Place the `rootca.crt` certificate in the directory you intend to run the client from.

If you get an error about SSL related files not being found despite them existing, shorten the names of the files.
If you cannot connect and the server prints a error related to TLSv1, ensure your version of boost and OpenSSL are up to date.

#### Covert Channel Modes
There are several covert channel modes which will transmit data in other ways. If you do not set this to any of the values below, the server will not have covert channel sending and recieving capabilities, but still answer to requests from clients. In this case, no superuser permissions will be required, as no network interfaces are touched directly.
`forward`: no data transmission<br/>
`tcpurgency`: uses the TCP urgency pointer<br/>
`tcpoptiontimestamp`: uses the TCP option Timestamp to transmit data. WARNING: most OSs use the timestamp so you should not use this option unless you are sure that the communication does not depend on it.<br/>
`tcpappend`: appends the data to the payload of a TCP packet<br/>
`tcpoptioncustom`: writes data in a custom option field<br/>

#### General Covert Channel options
`covertChannelMode`: Sets the covert channel mode. To deactivate don't set it or set it to none or false.<br/>
`innerInterface`: The interface of your inner network<br/>
`outerInterface`: The interface of your outer network<br/>

###### Covert Channel Mode `forward`
No further config is needed. Forward should work out of the box.<br/>

###### Covert Channel Modes `tcpurgency`, `tcpoptiontimestamp`, `tcpappend`, `tcpoptioncustom`
`targetIP`: IP of the target server<br/>
`targetPort`: Port of the target server<br/>
`passiveMode`: true - server only reacts to incoming channel | false - server initiates channel<br/>

### Example for config.txt
```
covertChannelMode=tcpurgency
deleteAllowed=false
filedirectory=./files/
innerInterface=eth0
outerInterface=eth1
passiveMode=false
port=1234
SSLcertificate=user.crt
SSLdhparams=dh2048.pem
SSLenabled=true
SSLprivatekey=user.key
targetIP=1.2.3.4
targetPort=443
userdatabase=userStorage.txt
```
