#ifndef JSONCOMMANDER_H
#define JSONCOMMANDER_H

#include <json/json.h>

#include "FileManager.h"

/**
 * @class JsonCommander
 *
 * A class to execute the json commands which were sent by a client.
 * It also uses the file manager for writing and reading files.
 */
class JsonCommander {
public:
	/**
	 * Action for the json response.
	 * send         - send the json answer
	 * closeAndSend - send the json answer and close the socket
	 * close        - close the socket
	 */
	enum Action { close, send, closeAndSend };

	/**
	 * Response for commands
	 *
	 * If action is set to close json can be uninitialized.
	 */
	struct Response {
		Action action;
		Json::Value json;
	};

	/**
	 * Creates a JsonCommaner
	 */
	JsonCommander(FileManager &fileManager);

	/**
	 * Deletes the JsonCommander
	 */
	~JsonCommander();

	/**
	 * Executes commands
	 */
	Response execute(const Json::Value &message);

	/**
	 * Does version check before login
	 */
	Response testVersion(const Json::Value &message);

	/**
	 * Checks if login is valid
	 */
	Response checkLogin(const Json::Value &message);

	/**
	 * protocol version of the client server protocol
	 */
	const int protocolMajorVersion = 0;
	const int protocolMinorVersion = 3;

private:
	/**
	 * Contains the name of the user which uses the current connection.
	 * Is set at checkLogin. Used for deleteUser.
	 */
	std::string currentUser;

	/**
	 * Map of all commands
	 *
	 * Used to find the command function fast
	 */
	std::map<std::string, Response (JsonCommander::*)(const Json::Value &)> commandsMap;

	/**
	 * file manager for reading and writing files
	 */
	FileManager &fileManager;

	/**
	 *
	 * Last chunk number which was sent.
	 */
	int getFileRemaining;

	/**
	 * Last chunk number which was received.
	 */
	int putFileReceived;

	/**
	 * Total chunk number from current put transfer
	 */
	int putSize;

	/**
	 * Total chunk number from current get transfer
	 */
	int getSize;

	/**
	 * Executes the status command
	 */
	Response executeStatus(const Json::Value &message);

	/**
	 * Executes the close command
	 */
	Response executeClose(const Json::Value &message);

	/**
	 * Executes the list command
	 */
	Response executeList(const Json::Value &message);

	/**
	 * Executes the listdata command
	 */
	Response executeListData(const Json::Value &message);

	/**
	 * Executes the put command
	 */
	Response executePut(const Json::Value &message);

	/**
	 * Executes the putdata command
	 */
	Response executePutdata(const Json::Value &message);

	/**
	 * Executes the get command
	 */
	Response executeGet(const Json::Value &message);

	/**
	 * Executes the getdata command
	 */
	Response executeGetdata(const Json::Value &message);

	/**
	 * Executes the head command
	 */
	Response executeHead(const Json::Value &message);

	/**
	 * Executes the deleteme command
	 */
	Response executeDeleteMe(const Json::Value &message);

	/**
	 * Executes the deletefile command
	 */
	Response executeDeleteFile(const Json::Value &message);

	/**
	 * Executes the extendedstatus command
	 */
	Response executeExtendedStatus(const Json::Value &message);

	/**
	 * Executes the notifications command
	 */
	Response executeNotifications(const Json::Value &message);

	/**
	 * Executes the extendedlist command
	 */
	Response executeExtendedList(const Json::Value &message);

	/**
	 * Executes the extendedlistdata command
	 */
	Response executeExtendedListData(const Json::Value &message);

	/**
	 * Executes the queue command
	 */
	Response executeQueue(const Json::Value &message);

	/**
	 * Executes the dequeue command
	 */
	Response executeDequeue(const Json::Value &message);
};

#endif
