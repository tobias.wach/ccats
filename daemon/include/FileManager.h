#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <fstream>
#include <tuple>
#include <vector>

/**
 * @class FileManager
 *
 * Manages file writes for uploads and file reads for downloads
 */
class FileManager {
private:
	/**
	 * Directory of the files uploaded
	 */
	const std::string fileDirectory;

	/**
	 * allows file deletion
	 */
	const bool deleteAllowed;

	/**
	 * file stream for get command
	 */
	std::ifstream getFile;

	/**
	 * file stream for put command
	 */
	std::ofstream putFile;

	/**
	 * file name for put command
	 * (used to delete the file if the upload is canceled)
	 */
	std::string putFileName;

	/**
	 * file name for put command
	 */
	std::string putBaseFileName;

	/**
	 * file name for get command
	 */
	std::string getBaseFileName;

	/*
	 * size of the get file
	 *
	 * Used to determine how much to read and encode as base64
	 */
	std::streamoff getFileSize;

	/**
	 * list vector for list command
	 */
	std::vector<std::vector<std::string>> list;

	/**
	 * extendedlist vector for extendend list command
	 * contains name head and size
	 */
	std::vector<std::vector<std::tuple<std::string, std::string, double>>> extendedlist;

public:
	enum { max_data_length = 4096 };

	/**
	 * FileManager Errors
	 *
	 * errors which the head or deleteFile command returns
	 */
	enum Error { no_error, no_such_file, file_too_small, not_allowed };

	/**
	 * Creates the file manager
	 */
	FileManager();

	/**
	 * Destroys the file manager
	 */
	~FileManager();

	/**
	 * Checks if an upload is running
	 * @return true - upload running | false - no upload
	 */
	virtual bool isUploading();

	/**
	 * Check if a download is running
	 * @return true - download running | false - no download
	 */
	virtual bool isDownloading();

	/**
	 * Opens put file if it doesn't exist
	 * @return true - file is open | false - file is not open
	 */
	virtual bool openPutFile(const std::string &filename);

	/**
	 * Opens get file if it exists and reports the amount of chunks
	 * @return true - file is open | false - file is not open
	 */
	virtual std::pair<bool, int> openGetFile(const std::string &filename);

	/**
	 * Closes file
	 */
	void closePutFile();

	/**
	 * Closes file
	 */
	void closeGetFile();

	/**
	 * Closes put file and deletes it
	 */
	virtual void cancelPut();

	/**
	 * Checks if a file name is valid
	 * @return true - name is valid | false - name is invalid
	 */
	bool checkFilename(const std::string &name);

	/**
	 * Return the name of the download file
	 * @return name of the download file
	 */
	virtual std::string getGetBaseFileName();

	/**
	 * Return the name of the upload file
	 * @return name of the upload file
	 */
	virtual std::string getPutBaseFileName();

	/**
	 * Writes data to put file
	 */
	virtual void writePut(const std::vector<char> &data);

	/**
	 * Reads data from get file
	 */
	virtual std::vector<char> readGet();

	/**
	 * Open list command. Set list vector and claculate chunks
	 *
	 * @return chunks of the resulting list | if a filename is too long it returns
	 * -1
	 */
	virtual int openList();

	/**
	 * @return remaining chunks to be send with listdata command
	 */
	virtual int getRemainingListChunks();

	/**
	 * @return size of the list vector
	 */
	virtual int getListSize();

	/**
	 * Return next chunk for listdata command.
	 * Increases remainingListChunks. You need to run openList before.
	 *
	 * @return next chnuk vector
	 */
	virtual std::vector<std::string> getNextChunkFromList();

	/**
	 * Cancel current list command.
	 * Clear list vector and set remainingListChunks zero.
	 */
	void cancelList();

	/**
	 * Get the first n bytes of a file as string.
	 * @param filename name of the files
	 * @param numOfBytes the number of bytes you want
	 *
	 * @return first: the array of chars containing the data if no error occured
	 *         second: an error code
	 */
	virtual std::pair<std::vector<char>, Error> getBytesFromFile(const std::string &filename, int numOfBytes);

	/**
	 * Delete a file in the file directory.
	 * @param filename name of the file
	 *
	 * @return return code 0: ok 1: disabled in config 2: file not found
	 */
	virtual Error deleteFile(const std::string &filename);

	/**
	 * Open extendedlist command. Set extendedlist vector and claculate chunks
	 *
	 * @return chunks of the resulting list | if a filename is too long it returns
	 * -1
	 */
	virtual int openExtendedList();

	/**
	 * Open extendedlist command. Set extendedlist vector and claculate chunks
	 *
	 * @return chunks of the resulting extendedlist | if a filename is too long it returns
	 * -1
	 */
	virtual int getRemainingExtendedListChunks();

	/**
	 * Return next chunk for extendedlistdata command.
	 * You need to run openList before.
	 *
	 * @return next chnuk vector
	 */
	virtual std::vector<std::tuple<std::string, std::string, double>> getNextChunkFromExtendedList();

	/**
	 * @return size of the extendendlist vector
	 */
	virtual int getExtendedListSize();

	/**
	 * Cancel current extendendlist command.
	 * Clear extendendlist vector and set remainingListChunks zero.
	 */
	virtual void cancelExtendedList();
};

#endif
