#ifndef SERVER_H
#define SERVER_H

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/bind.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <json/json.h>

#include "JsonCommander.h"

using namespace boost::asio;
using ip::tcp;

/**
 * @class con_handler
 *
 * Handler for a connection.
 *
 * Handles and manages a connection between client and server.
 */
class con_handler : public boost::enable_shared_from_this<con_handler> {
private:
	/**
	 * boost tcp socket
	 */
	tcp::socket sock;

	/**
	 * ssl stream socket
	 */
	ssl::stream<tcp::socket &> sslsock;

	/**
	 * ssl enable state
	 *
	 * true - ssl is enabled | false ssl is disabled
	 */
	const bool usessl;

	/**
	 * Performs SSL handshake.
	 *
	 * @return true - success | false - failure
	 */
	bool handshake();

	/**
	 * Closes socket
	 */
	void close_sock();

	/**
	 * max buffer length
	 */
	enum { max_length = 16384 };

	/**
	 * data buffer
	 */
	streambuf buf;

	/**
	 * string builder for json
	 */
	Json::StreamWriterBuilder jsonStringBuilder;

	/**
	 * json reader to parse json strings
	 */
	std::unique_ptr<Json::CharReader> jsonReader;

	/**
	 * Executes json commands
	 */
	JsonCommander jsonCommander;

	/**
	 * File manager used by jsonCommander
	 */
	FileManager fileManager;

	/**
	 * Reads data and binds it to a handler.
	 *
	 * @param handler method pointer to a handler_read_* method
	 */
	void read(void (con_handler::*handler)(const boost::system::error_code &err, size_t bytes_transferred));

	/**
	 * Sends json data over the network.
	 *
	 * @param json json object
	 */
	void sendJson(const Json::Value &json);

	/**
	 * Parses a line of the buffer to a json value object.
	 *
	 * @return json object
	 */
	Json::Value parseMessage();

public:
	/**
	 * Pointer to a con_handler.
	 */
	typedef boost::shared_ptr<con_handler> pointer;

	/**
	 * con_handler constructor
	 *
	 * Creates a con_handler
	 *
	 * @param io_service connection info
	 */
	con_handler(basic_socket_acceptor<ip::tcp>::executor_type &io_service, boost::asio::ssl::context &context);

	/**
	 * con_handler destructor.
	 */
	~con_handler();

	/**
	 * Creates a new con_handler.
	 *
	 * Allocates a new con_hanlder and returns the pointer.
	 *
	 * @param io_service connection info
	 *
	 * @return con_handler pointer
	 */
	static pointer create(basic_socket_acceptor<ip::tcp>::executor_type &io_service, boost::asio::ssl::context &context);

	/**
	 * socket getter
	 *
	 * Returns the socket of the connection.
	 *
	 * @return the socket
	 */
	tcp::socket &socket();

	/**
	 * Starts a connection.
	 *
	 * Negotiates a connection and sets everything important to the connection up.
	 */
	void start();

	/**
	 * Read handler
	 *
	 * Will be called if the server expected a version and received something.
	 *
	 * @param err error
	 * @param bytes_transferred amount of transferred bytes
	 */
	void handle_read_version(const boost::system::error_code &err, size_t bytes_transferred);

	/**
	 * Read handler
	 *
	 * Will be called after the version was verifyed. Checks login data.
	 *
	 * @param err error
	 * @param bytes_transferred amount of transferred bytes
	 */
	void handle_read_login(const boost::system::error_code &err, size_t bytes_transferred);

	/**
	 * Read handler
	 *
	 * Will be called after the version and login was verifyed. Executes commands.
	 *
	 * @param err error
	 * @param bytes_transferred amount of transferred bytes
	 */
	void handle_read_command(const boost::system::error_code &err, size_t bytes_transferred);

	/**
	 * Write handler
	 *
	 * Will be called if the server sends something.
	 *
	 * @param err error
	 * @param bytes_transferred amount of transferred bytes
	 */
	void handle_write(const boost::system::error_code &err, size_t bytes_transferred);
};

/**
 * @class Server
 *
 * boost server
 *
 * boost server which creates the handler for the connections.
 */
class Server {
private:
	tcp::acceptor acceptor_;
	boost::asio::ssl::context context_;

	/**
	 * Accepts traffic
	 */
	void start_accept();

public:
	/**
	 * Creates a boost server
	 *
	 * @param io_service connection info
	 */
	Server(io_service &io_service);

	/**
	 * Destroys the server
	 */
	~Server();

	/**
	 * Handles traffic?
	 *
	 * @param connection con_handler
	 * @param err error
	 */
	void handle_accept(con_handler::pointer connection, const boost::system::error_code &err);
};

#endif
