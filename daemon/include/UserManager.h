#ifndef USER_MANAGER_H
#define USER_MANAGER_H

#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

/**
 * @Class UserManager
 *
 * Handle differnt users with passwords and store them
 */
class UserManager {
public:
	/**
	 * Makes shure that there is a user storage.
	 * If not. It creates a new one
	 */
	static void init(const std::string &file);

	/**
	 * Checks if a given user with password is valid
	 *
	 * @param user the name of the users
	 * @param pw password in plain text
	 */
	static bool isAllowed(const std::string &user, const std::string &pw);

	/**
	 * Add a new user and write to storage.
	 *
	 * @param user the name of the users
	 * @param pw password in plain text
	 */
	static bool addUser(const std::string &name, const std::string &pw);

	/**
	 * Delete a new user and delete from storage.
	 *
	 * @param user the name of the users
	 * @param pw password in plain text
	 */
	static bool deleteUser(const std::string &name, const std::string &pw);

private:
	/**
	 * Name of the file which holds the user data
	 */
	static std::string filename;

	/**
	 * Read data from file and create map.
	 *
	 * @param user_map pointer to the map
	 */
	static void readFromFile(std::map<std::string, std::string> &user_map);

	/**
	 * Write data to afile
	 *
	 * @param user_map pointer to the map
	 */
	static void writeToFile(std::map<std::string, std::string> &user_map);
};

#endif
