#ifndef QUEUE_H
#define QUEUE_H

#include <deque>
#include <mutex>
#include <string>

#include "CovertChannel/ChannelControls.h"

/**
 * Namespace wich manages the queue for the covert channel
 */
namespace Queue {
/**
 * Adds file to queue
 * @param file add file to queue
 */
bool push(const std::string &file);

/**
 * Remove file from queue. Checks if remove is allowed
 * @param file file to remove from queue
 */
bool remove(const std::string &file);

/**
 * Schedules one file to be transfered over the covert channel.
 * schedule() must be called again to send the next file.
 */
void schedule();

/**
 * Returns the name of the file of the current transfer. Empty otherwise.
 * @return file name
 */
std::string curTransfer();

/**
 * The queue. Stores filenames. Acts as FIFO.
 */
extern std::deque<std::string> queue;

/**
 * Mutex to lock queue while editing it.
 */
extern std::mutex mtx;

/**
 * Covert channel which will be used to send files.
 */
extern ChannelControls *channel;
}; // namespace Queue

#endif // QUEUE_H
