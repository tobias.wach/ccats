#ifndef TCPURGENCYCHANNEL_H
#define TCPURGENCYCHANNEL_H

#include "../BidirectionalChannels.hpp"

/**
 * @class TCPUrgencyChannel
 *
 * A CovertChannel which hides data in the TCP urgency pointer
 *
 * @param PASSIVE true - server only reacts to incoming channel | false - server initiates channel
 */
template <bool PASSIVE> class TCPUrgencyChannel : public BidirectionalChannels<2, PASSIVE> {
public:
	/**
	 * Sets up a CovertChannel.
	 *
	 * Creates a CovertChannel, sets the network interfaces for sniffing and sending and sets the filter.
	 *
	 * @param innerInterface name of the interface of the inner network
	 * @param outerInterface name of the interface of the outer network
	 * @param targetIP IP of the target server
	 * @param targetPort Port of the target server
	 */
	TCPUrgencyChannel(const std::string &innerInterface, const std::string &outerInterface, const std::string &targetIP, const std::string &targetPort)
	    : BidirectionalChannels<2, PASSIVE>(innerInterface, outerInterface, targetIP, targetPort) {}

	/**
	 * Destroys the CovertChannel.
	 */
	virtual ~TCPUrgencyChannel() {}

protected:
	/**
	 * Handler for sniffed packets filterd to forward from the outer network.
	 *
	 * Handles incoming packets and forwards them.
	 *
	 * @param pdu sniffed packet
	 *
	 * @return false = stop loop | true = continue loop
	 */
	virtual bool handleChannelFromOuter(Tins::PDU &pdu) {
		Tins::TCP &tcp = pdu.rfind_pdu<Tins::TCP>();

		uint16_t data = tcp.urg_ptr();
		BidirectionalChannels<2, PASSIVE>::protocol.receive((uint8_t *)(&data));
		tcp.urg_ptr(0);
		tcp.set_flag(Tins::TCP::Flags::URG, false);
		BidirectionalChannels<2, PASSIVE>::innerSender.send(pdu);

		return true;
	}

	/**
	 * Handler for sniffed packets filterd to forward from the inner network.
	 *
	 * Handles incoming packets and forwards them.
	 *
	 * @param pdu sniffed packet
	 *
	 * @return false = stop loop | true = continue loop
	 */
	virtual bool handleChannelFromInner(Tins::PDU &pdu) {
		Tins::TCP &tcp = pdu.rfind_pdu<Tins::TCP>();

		uint16_t data = 0;
		BidirectionalChannels<2, PASSIVE>::protocol.send((uint8_t *)(&data));
		tcp.urg_ptr(data);
		tcp.set_flag(Tins::TCP::Flags::URG, true);
		BidirectionalChannels<2, PASSIVE>::outerSender.send(pdu);

		return true;
	}
};

#endif
