#ifndef COVERTCHANNEL_H
#define COVERTCHANNEL_H

#include <thread>
#include <tins/tins.h>

#include "ChannelControls.h"

/**
 * @class CovertChannel
 *
 * Sniffs the network, redirects traffic and handles filtered traffic.
 *
 * CovertChannel class which will sniff on two network interfacees. It handles filtered traffic with a virtual handler
 * function.
 */
class CovertChannel : public ChannelControls {
public:
	/**
	 * Sets up a CovertChannel.
	 *
	 * Creates a CovertChannel, sets the network interfaces for sniffing and sending and sets the filter.
	 *
	 * @param innerInterface name of the interface of the inner network
	 * @param outerInterface name of the interface of the outer network
	 * @param innerForwardFilter pcap filter string which will be set for the channel sniffers and negated for the forward sniffers
	 * @param outerForwardFilter pcap filter string which will be set for the channel sniffers and negated for the forward sniffers
	 * @param innerChannelFilter pcap filter string which will be set for the channel sniffers and negated for the forward sniffers
	 * @param outerChannelFilter pcap filter string which will be set for the channel sniffers and negated for the forward sniffers
	 */
	CovertChannel(const std::string &innerInterface, const std::string &outerInterface, const std::string &innerForwardFilter = "",
	              const std::string &outerForwardFilter = "", const std::string &innerChannelFilter = "", const std::string &outerChannelFilter = "");

	/**
	 * Destroys the CovertChannel.
	 */
	virtual ~CovertChannel();

	/**
	 * Start sniffing on the interface.
	 *
	 * Starts a sniffing loop which calls handle. The loop will only be stopped if
	 * handle returns false.
	 */
	void startSniffing();

	/**
	 * Sets a filter for the sniffers.
	 *
	 * Sets the filter for the forward and channel sniffers with a pcap filter string. E.g. "host 8.8.8.8".
	 * The forward filter is the negated filter.
	 *
	 * @param filter pcap filter string which will be set for the channel sniffers and negated for the forward sniffers
	 */
	void setFilter(const std::string &innerForwardFilter = "", const std::string &outerForwardFilter = "", const std::string &innerChannelFilter = "",
	               const std::string &outerChannelFilter = "");

protected:
	/**
	 * Handler for sniffed packets filterd to forward from the outer network.
	 *
	 * Handles incoming packets and forwards them.
	 *
	 * @param pdu sniffed packet
	 *
	 * @return false = stop loop | true = continue loop
	 */
	bool handleForwardFromOuter(Tins::PDU &pdu);

	/**
	 * Handler for sniffed packets filterd to forward from the inner network.
	 *
	 * Handles incoming packets and forwards them.
	 *
	 * @param pdu sniffed packet
	 *
	 * @return false = stop loop | true = continue loop
	 */
	bool handleForwardFromInner(Tins::PDU &pdu);

	/**
	 * Handler for sniffed packets filterd to use as channel from the outer network.
	 *
	 * Handles incoming packets and redirets them.
	 *
	 * @param pdu sniffed packet
	 *
	 * @return false = stop loop | true = continue loop
	 */
	virtual bool handleChannelFromOuter(Tins::PDU &pdu) = 0;

	/**
	 * Handler for sniffed packets filterd to use as channel from the outer network.
	 *
	 * Handles incoming packets and redirets them.
	 *
	 * @param pdu sniffed packet
	 *
	 * @return false = stop loop | true = continue loop
	 */
	virtual bool handleChannelFromInner(Tins::PDU &pdu) = 0;

	/**
	 * Starts the sniffing loop of the inner forward sniffer.
	 */
	void startInnerForwardSniffing();

	/**
	 * Starts the sniffing loop of the outer forward sniffer.
	 */
	void startOuterForwardSniffing();

	/**
	 * Starts the sniffing loop of the inner channel sniffer.
	 */
	void startInnerChannelSniffing();

	/**
	 * Starts the sniffing loop of the outer channel sniffer.
	 */
	void startOuterChannelSniffing();

	/**
	 * Tins Sniffer to filter packets to which should be forwarded
	 */
	Tins::Sniffer *innerForwardSniffer;

	/**
	 * Tins Sniffer to filter packets to which should be forwarded
	 */
	Tins::Sniffer *outerForwardSniffer;

	/**
	 * Tins Sniffer to filter packets to which should be used for the covert channel
	 */
	Tins::Sniffer *innerChannelSniffer;

	/**
	 * Tins Sniffer to filter packets to which should be used for the covert channel
	 */
	Tins::Sniffer *outerChannelSniffer;

	/**
	 * Tins PacketSender which sends packets to the inner network
	 */
	Tins::PacketSender innerSender;

	/**
	 * Tins PacketSender which sends packets to the outer network
	 */
	Tins::PacketSender outerSender;

private:
	/**
	 * Thread for the inner forward sniffer
	 */
	std::thread innerForwardSnifferThread;

	/**
	 * Thread for the outer forward sniffer
	 */
	std::thread outerForwardSnifferThread;

	/**
	 * Thread for the inner channel sniffer
	 */
	std::thread innerChannelSnifferThread;

	/**
	 * Thread for the outer channel sniffer
	 */
	std::thread outerChannelSnifferThread;
};

#endif
