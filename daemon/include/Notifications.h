#ifndef NOTIFICATIONS_H
#define NOTIFICATIONS_H

#include <map>
#include <string>
#include <vector>

/**
 * Namespace which holds notifications
 */
namespace Notifications {
/**
 * Add a new notification. Will be stored in messages and delivered by getMessages
 * @param message the new notification
 */
void newNotification(const std::string &message);

/**
 * Get all messages for a given users
 * Deletes all messages wihch are older than a week
 * @param user
 * @return vector with all messages
 */
std::vector<std::string> getMessages(const std::string &user);

/**
 * Stores the notifications
 * First is the timestamp
 * Second is the message
 */
extern std::vector<std::pair<long int, std::string>> messages;

/**
 * Stores information when a user get messages for the last time
 */
extern std::map<std::string, long int> userTimeStamps;
}; // namespace Notifications

#endif
