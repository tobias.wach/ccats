#!/bin/bash
ROOTNAME=rootca
USERNAME=user
DIFFIENAME=dh2048
echo "Creating ROOTCA key"
openssl genrsa -out $ROOTNAME.key 2048
echo "Creating ROOTCA cert"
openssl req -x509 -new -nodes -key $ROOTNAME.key -days 20000 -out $ROOTNAME.crt
echo "Creating USER key"
openssl genrsa -out $USERNAME.key 2048
echo "Creating USER base cert"
openssl req -new -key $USERNAME.key -out $USERNAME.csr
echo "Signing USER base cert using ROOT key and cert"
openssl x509 -req -in $USERNAME.csr -CA $ROOTNAME.crt -CAkey $ROOTNAME.key -CAcreateserial -out $USERNAME.crt -days 20000
echo "Creating DIFFIE params. This might take a while"
openssl dhparam -out $DIFFIENAME.pem 2048

# should OK
echo "This should OK"
openssl verify -CAfile $ROOTNAME.crt $ROOTNAME.crt
# should OK
echo "This should OK"
openssl verify -CAfile $ROOTNAME.crt $USERNAME.crt
# should FAIL
echo "This should FAIL"
openssl verify -CAfile $USERNAME.crt $USERNAME.crt
