#include "../include/batchioman.h"
#include "../include/machineioman.h"
#include "../include/userioman.h"

#include <boost/program_options.hpp>
#include <iostream>
#include <string>

namespace bpo = boost::program_options;

/* this is provided for the readline callback in IoMan */
IoMan *gIOMAN = NULL;

void show_help(char *exec) {
	std::printf("ccats command line interface\n");
	std::printf("usage: %s [Options]\n", exec);
}

int main(int argc, char **argv) {
	bpo::options_description desc("Options");
	desc.add_options()("help", "show this help")("machine", "switch to machine mode for I/O (not designed for user interaction)")(
	    "batch", bpo::value<std::string>(), "run operations from arg as batch file")("usessl", bpo::value<std::string>(),
	                                                                                 "enable ssl for connection to server")("verbose", "enable debug output");

	bpo::variables_map vm;
	bool machine = false, usessl = false, batch = false, verbose = false;
	const char *file = NULL;
	const char *certfile = NULL;
	IoMan *ioman;

	try {
		store(parse_command_line(argc, argv, desc), vm);
		notify(vm);

		if (vm.count("help")) {
			show_help(argv[0]);
			std::cout << desc;
			return 1;
		}
		if (vm.count("machine")) {
			// enable machine/gui mode
			machine = true;
		}
		if (vm.count("batch")) {
			// handle batch file mode
			file = vm["batch"].as<std::string>().c_str();
			batch = true;
		}
		if (vm.count("usessl")) {
			certfile = vm["usessl"].as<std::string>().c_str();
			usessl = true;
		}
		if (vm.count("verbose")) {
			verbose = true;
		}
	} catch (const bpo::error &ex) {
		std::cerr << ex.what() << std::endl;
		return 1;
	}
	std::printf("machine mode is %d file is %s enablessl is %d verbose is %d\n", machine, file ? file : "", usessl, verbose);
	if (batch) {
		ioman = new BatchIoMan(usessl, certfile, verbose, file);
	} else if (machine) {
		ioman = new MachineIoMan(usessl, certfile, verbose);
	} else {
		ioman = new UserIoMan(usessl, certfile, verbose);
	}
	gIOMAN = ioman;
	if (ioman->init()) {
		ioman->run();
	}
	delete ioman;
	gIOMAN = NULL;
	std::printf("done\n");
	return 0;
}
