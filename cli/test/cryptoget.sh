#!/bin/bash

SCRIPT_PATH=${0%/*}
if [ "$0" != "$SCRIPT_PATH" ] && [ "$SCRIPT_PATH" != "" ]; then
    cd "$SCRIPT_PATH/.."
fi

DAEMONFILEPATH="../../daemon/build/files"

if [ $# -eq 1 ];
	then DAEMONFILEPATH="$1"
fi

echo "am in directory $PWD"
echo "using file path $DAEMONFILEPATH"

test/cryptotest_gcm 1 test/samplekey1.bin test/samplefile.txt $DAEMONFILEPATH/samplefile.txt

bin/ccats-cli --batch test/cryptoget.txt

if [ ! -f test/cryptoget.txt.out ];
then
	echo "running of batch file failed"
	exit 1;
fi

if [ ! -f samplefile.txt ];
then
	echo "file didnt download"
	echo "STDOUT is :"
	cat test/cryptoget.txt.out
	echo "STDERR is :"
	cat test/cryptoget.txt.err
	exit 1;
fi

diff samplefile.txt test/samplefile.txt
DIFFRES=$?

rm $DAEMONFILEPATH/samplefile.txt
rm samplefile.txt

if [ $DIFFRES -ne 0 ];
then
	echo "files are not equal after decryption: $DIFFRES"
	exit 1;
fi

exit 0;
