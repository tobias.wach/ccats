#!/bin/bash

SCRIPT_PATH=${0%/*}
if [ "$0" != "$SCRIPT_PATH" ] && [ "$SCRIPT_PATH" != "" ]; then
    cd "$SCRIPT_PATH/.."
fi

bin/ccats-cli --batch test/logintest_pos.txt

if [ ! -f test/logintest_pos.txt.out ];
then
	exit 1;
fi
if [ -z "$(grep "Login ok." test/logintest_pos.txt.out)" ];
then
	exit 1;
fi
exit 0;
