#!/bin/bash

SCRIPT_PATH=${0%/*}
if [ "$0" != "$SCRIPT_PATH" ] && [ "$SCRIPT_PATH" != "" ]; then
    cd "$SCRIPT_PATH/.."
fi

if [ $# -eq 1 ];
	then DAEMONFILEPATH="$1"
fi

bin/ccats-cli --batch test/logintest_neg.txt

if [ ! -f test/logintest_neg.txt.err ];
then
	exit 1;
fi
if [ -z "$(grep "Login failed" test/logintest_neg.txt.err)" ];
then
	exit 1;
fi
exit 0;
