#include "../include/cmdman.h"

class CmdManForTest : public CmdMan {
public:
	CmdManForTest(FileMan &fm, void (*dpf)(string)) : CmdMan(fm, dpf) {}

	/*
	 * initialize state
	 */
	void initNotConnected() { currentState = connectionpossible; }

	void initConnected() { currentState = versionpossible; }

	void initVersionChecked() { currentState = loginpossible; }

	void initLoggedIn() { currentState = normal; }

	/*
	 * check state
	 */

	bool isNotConnected() { return currentState == connectionpossible; }

	bool isConnected() { return currentState == versionpossible; }

	bool isVersionChecked() { return currentState == loginpossible; }

	bool isLoggedIn() { return currentState == normal; }

	// connectionpossible, versionpossible, doversion, loginpossible, dologin, dosignup, normal, disconnecttoexit, disconnecttoexitearly
};
