#ifndef FILEMAN_MOCK_H
#define FILEMAN_MOCK_H

#include "../include/fileman.h"
#include <gmock/gmock.h>

class FileManMock : public FileMan {
public:
	MOCK_METHOD(bool, isGetting, (), (override));
	MOCK_METHOD(bool, isPutting, (), (override));
	MOCK_METHOD(bool, isListing, (), (override));
	MOCK_METHOD(bool, isListingSimple, (), (override));
	MOCK_METHOD(bool, isListingExtended, (), (override));

	MOCK_METHOD(bool, openPut, (const std::string &path), (override));
	MOCK_METHOD(bool, openGet, (const std::string &path), (override));
	MOCK_METHOD(bool, openList, (bool extended), (override));

	MOCK_METHOD(void, closePut, (), (override));
	MOCK_METHOD(void, closeGet, (), (override));
	MOCK_METHOD(void, closeList, (), (override));

	MOCK_METHOD(std::string, getPutName, (), (override));
	MOCK_METHOD(std::string, getGetName, (), (override));

	MOCK_METHOD(void, cancelPut, (), (override));
	MOCK_METHOD(void, cancelGet, (), (override));
	MOCK_METHOD(void, cancelList, (), (override));

	MOCK_METHOD(std::vector<char>, readPut, (), (override));

	MOCK_METHOD(void, writeGet, (std::vector<char> data), (override));

	MOCK_METHOD(std::string, readBase64, (), (override));
	MOCK_METHOD(void, writeBase64, (std::string data), (override));

	MOCK_METHOD(void, putListData, (std::vector<Json::Value> names), (override));
	MOCK_METHOD(std::vector<Json::Value>, getListData, (), (override));

	MOCK_METHOD(int, getPutChunks, (), (override));
	MOCK_METHOD(int, getGetChunks, (), (override));
	MOCK_METHOD(int, getListChunks, (), (override));
	MOCK_METHOD(int, getPutRemainingChunks, (), (override));
	MOCK_METHOD(int, getGetRemainingChunks, (), (override));
	MOCK_METHOD(int, getListRemainingChunks, (), (override));
	MOCK_METHOD(int, getPutSize, (), (override));

	MOCK_METHOD(void, setGetChunks, (int chunks), (override));
	MOCK_METHOD(void, setListChunks, (int chunks), (override));

	MOCK_METHOD(std::string, pathToFilename, (std::string path), (override));

	MOCK_METHOD(std::string, getOpensslError, (), (override));
	MOCK_METHOD(bool, openKey, (const std::string &path), (override));

	MOCK_METHOD(bool, closeKey, (), (override));
};

#endif
