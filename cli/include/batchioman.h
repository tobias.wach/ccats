#ifndef BATCHIOMAN_H
#define BATCHIOMAN_H

#include "ioman.h"
#include <fstream>
#include <string>
#include <vector>

/**
 * @class BatchIoMan
 *
 * Provides specific implementations of IoMan outputs and prompts
 * for unattended sessions
 */
class BatchIoMan : public IoMan {
private:
	/**
	 * Map containing pointers to the appropriate member functions for printing
	 * formatted json output
	 */
	map<string, std::string (BatchIoMan::*)(Json::Value)> printmap;

	/**
	 * Class-wide json functionality
	 */
	Json::CharReader *reader;
	Json::StreamWriterBuilder wbuilder;
	string jsonerror;

	std::ofstream normalout;
	std::ofstream errorout;
	std::ofstream debugout;
	std::ifstream batchin;

	/**
	 * Mutex for synchronized message output
	 */
	std::recursive_mutex msgmutex;

	bool getnextline;
	bool verbose;
	string filepath;
	std::mutex linemutex;
	std::condition_variable linecv;

	/**
	 * Format and pretty print json for logfile output
	 */
	std::string printJson(Json::Value root);

	/**
	 * Method prototypes for printing json output
	 */
	/* printing commands go here */
	std::string printError(Json::Value root);
	std::string printConnect(Json::Value root);
	std::string printHelp(Json::Value root);
	std::string printStatus(Json::Value root);
	std::string printExtendedstatus(Json::Value root);
	std::string printDisconnect(Json::Value root);
	std::string printPut(Json::Value root);
	std::string printGet(Json::Value root);
	std::string printList(Json::Value root);
	std::string printExtendedlist(Json::Value root);
	std::string printVersion(Json::Value root);
	std::string printLogin(Json::Value root);
	std::string printSignup(Json::Value root);
	std::string printPutdata(Json::Value root);
	std::string printGetdata(Json::Value root);
	std::string printListdata(Json::Value root);
	std::string printHead(Json::Value root);
	std::string printDeletefile(Json::Value root);
	std::string printQueue(Json::Value root);
	std::string printDequeue(Json::Value root);
	std::string printDeleteme(Json::Value root);
	std::string printKeyfile(Json::Value root);
	std::string printClosekey(Json::Value root);
	std::string printNotifications(Json::Value root);

public:
	/**
	 * Constructor and destructor
	 */
	BatchIoMan(bool usessl, const char *certfile, bool beverbose, string batchpath);
	~BatchIoMan();

	bool init();
	void run();

protected:
	/**
	 * Specific implementations for printing messages
	 */
	void printMessage(std::string msg, OutMsgType type);
	void printWelcomeMessage();
	void handleInCmdResponse(CmdMan::CmdRet cmdret);
	void handleOutCmdResponse(CmdMan::CmdRet cmdret, std::vector<std::string> &toput);

	/**
	 * Return the specific prompt strings for IoMan prompts
	 */
	std::string getCmdPrompt();
};

#endif
