#ifndef MACHINEIOMAN_H
#define MACHINEIOMAN_H

#include "ioman.h"

/**
 * @class MachineIoMan
 *
 * Provides specific implementations of IoMan outputs and prompts
 * for interactive non-user sessions
 */
class MachineIoMan : public IoMan {
private:
	bool verbose;

public:
	MachineIoMan(bool usessl, const char *certfile, bool beverbose);

protected:
	/**
	 * Specific implementations for printing messages
	 */
	void printMessage(std::string msg, OutMsgType type);
	void printWelcomeMessage();

	/**
	 * Return the specific prompt strings for IoMan prompts
	 */
	std::string getCmdPrompt();
};

#endif
