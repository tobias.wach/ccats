#ifndef FILEMAN_H
#define FILEMAN_H

#include <fstream>
#include <json/json.h>
#include <openssl/conf.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include <string>
#include <vector>

/**
 * @class FileMan
 *
 * Provides File I/O abstraction
 */
class FileMan {
private:
	/**
	 * Internal state
	 *
	 * Filestreams for put and get
	 * Vector for holding received filenames from listing
	 * Paths and filenames for put and get
	 * Size for internal read
	 * Total and Remaining chunks for put, get and list
	 * Boolean replacement for filestreams being open for list
	 *
	 */
	std::ifstream putfile;
	std::fstream getfile;
	std::vector<Json::Value> listdata;
	std::string getpath, getname, putpath, putname, cipherpath;
	const unsigned int max_read_len = 4096;
	int putsize;
	int putchunks;
	int putchunksRemaining;
	int getchunks;
	int getchunksRemaining;
	int listchunks;
	int listchunksRemaining;

	bool isputting;
	bool islisting;
	bool keyenabled;
	bool cryptoreadye;
	bool cryptoreadyd;
	bool pendingerr;

	bool extendedListing;

	unsigned char iv[12];  // 96bits
	unsigned char tag[16]; // 128bits
	unsigned char key[32]; // 256bits
	const unsigned int cipherblocklen = 128;

	EVP_CIPHER_CTX *cryptctxe;
	EVP_CIPHER_CTX *cryptctxd;
	std::string opensslerr;

	void setOpensslError();
	bool initCryptoE();
	bool initCryptoD();
	void deinitCryptoE();
	void deinitCryptoD();

	void writeEnc(const std::vector<char> data);
	const char signature[4] = {'C', 'C', 'A', 'T'};

	enum decryptability { unknown, plaintext, decryptable, undecryptable };
	decryptability isDecryptable(const std::vector<char> data);

public:
	/**
	 * Constructor and Destructor
	 */
	FileMan();
	~FileMan();

	/**
	 * Query internal state
	 *
	 * Return true if the corresponding action is being performed, false otherwise.
	 * isListing returns true if either a list from the "list" or "extendedlist" is built.
	 * isListingExtended corresponds to the "extendedlist" command, isListingSimple to "list".
	 */
	virtual bool isGetting();
	virtual bool isPutting();
	virtual bool isListing();
	virtual bool isListingSimple();
	virtual bool isListingExtended();
	virtual bool isEncrypted();

	/**
	 * Check for and prepare state and streams for reading/writing
	 *
	 * Return true if successful, false otherwise
	 */
	virtual bool openPut(const std::string &path);
	virtual bool openGet(const std::string &path);
	virtual bool openList(bool extended);

	/**
	 * Open file and read a hex string from it as key and initialize the IV
	 * This will fail is a put or a get is in progress
	 *
	 * Return true if successful, false otherwise. After this put and get will en/decrypt data
	 */
	virtual bool openKey(const std::string &path);

	/**
	 * Close the respective filestream
	 */
	virtual void closePut();
	virtual void closeGet();
	virtual void closeList();

	/**
	 * Reset internal key state and disable en/decryption of data
	 * This will fail if a put or get is in progress
	 *
	 * Return true if key was reset, false otherwise. After this put and get will use unencrypted data
	 */
	virtual bool closeKey();

	/**
	 * Query the names of the file currently being put or get
	 */
	virtual std::string getPutName();
	virtual std::string getGetName();

	/**
	 * Cancel a put, get or list, depreparing internal state (closing streams if
	 * required)
	 */
	virtual void cancelPut();
	virtual void cancelGet();
	virtual void cancelList();

	/**
	 * Read max_rea_len bytes from the current file opened for put
	 */
	virtual std::vector<char> readPut();
	/**
	 * Write the provided vector to the current file opened for get
	 */
	virtual void writeGet(std::vector<char> data);

	/**
	 * Wrapper methods for reading and writing base64 encoded data instead of raw
	 * bytes
	 */
	virtual std::string readBase64();
	virtual void writeBase64(std::string data);

	/**
	 * read and write emulating methods for list
	 */
	virtual void putListData(std::vector<Json::Value> names);
	virtual std::vector<Json::Value> getListData();

	/**
	 * Query internal state, requesting the corresponding size
	 */
	virtual int getPutChunks();
	virtual int getGetChunks();
	virtual int getListChunks();
	virtual int getPutRemainingChunks();
	virtual int getGetRemainingChunks();
	virtual int getListRemainingChunks();
	virtual int getPutSize();

	/**
	 * Set internal state, adjusting the chunks as well as chunks remaining for
	 * get and list
	 */
	virtual void setGetChunks(int chunks);
	virtual void setListChunks(int chunks);

	/**
	 * Returns the filename of the passed (relative) path of a file
	 */
	virtual std::string pathToFilename(std::string path);

	virtual std::string getOpensslError();
};

#endif
