#ifndef CMDMAN_H
#define CMDMAN_H

#include "fileman.h"

#include <json/json.h>

#include <map>
#include <mutex>
#include <string>
#include <vector>

using std::map;
using std::string;
using std::vector;

/**
 * @class CmdMan
 *
 * A class that provides handling of user provided commands as well as
 * commands as provided by json responses from a server.
 */
class CmdMan {
public:
	/**
	 * Flags for type of message returned in CmdRet.
	 * print			- print something to the user
	 * send				- send something to the server
	 * error			- an error occured, do not send to the server
	 * close			- terminate the connection
	 * connect			- connect to the server
	 * exit				- exit the program
	 * noanswerexpected	- do not expect an answer from the server
	 */
	enum rettype {
		none = 0,
		print = (1 << 1),
		send = (1 << 2),
		error = (1 << 3),
		close = (1 << 4),
		connect = (1 << 5),
		exit = (1 << 6),
		noanswerexpected = (1 << 7)
	};
	/**
	 * Response to user or command input
	 *
	 * msg is to be handled depending on type
	 * if nextcommand isnt an empty string it should be handled;
	 */
	struct CmdRet {
		unsigned int type;
		string nextcommand;
		Json::Value msg;
	};
	/**
	 * Constructor and destructor
	 */
	CmdMan(FileMan &fm, void (*dpf)(string));
	~CmdMan();

	/**
	 * Executes a user provided command with optional arguments
	 */
	CmdRet execute(string cmd, vector<string> args);
	/**
	 * Handles a server provided response json
	 */
	CmdRet handle(Json::Value root);

	/**
	 * Internal json reader
	 */
	Json::CharReader *reader;

	/**
	 * Used to inform the CmdMan that the CLI is now (dis-)connected to the server.
	 * Sets the internal state of the CmdMan accordingly.
	 */
	void stateSetConnectionOk();
	void stateSetDisconnected();

protected:
	/**
	 * State used to internally format received json to allow easy handling using
	 * handlemap and to disallow the use of certain commands when logged in/not
	 * logged in.
	 */
	enum state { connectionpossible, versionpossible, doversion, loginpossible, dologin, dosignup, normal, disconnecttoexit, disconnecttoexitearly };
	state currentState;

private:
	/**
	 * Prevents multiple calls of execute and handle at the same time.
	 * Thereby prevents an incorrect state of used cmdman and fileman instances.
	 */
	std::mutex cmdmutex;

	/**
	 * internal json writer and error string member
	 */
	Json::StreamWriterBuilder wbuilder;
	string jsonerror;

	/**
	 * FileMan instance used to file commands
	 */
	FileMan &fileman;

	void (*debugprintfunc)(string);

	/**
	 * Maps containing pointers to the appropriate member functions for executing
	 * or handling commands
	 */
	map<string, CmdRet (CmdMan::*)(vector<string>)> execmap;
	map<string, CmdRet (CmdMan::*)(Json::Value)> handlemap;
	/**
	 * Map containing help strings to show to a user
	 */
	map<string, string> helpmap;

	/**
	 * Vectors containing command strings that should either be usable in any
	 * state or after connecting but not logging in
	 */
	vector<string> cmdAllowAlways = {"help", "keyfile", "closekey", "exit", "status"};
	vector<string> cmdAllowAfterConnect = {"login", "signup", "disconnect"};

	/**
	 * Fields used for status output.
	 */
	string username, ip;
	unsigned int port;

	/**
	 * Help strings and method prototypes for commands to be used by a user
	 */
	/* execute command descriptions and methods go here */
	const string descHelp = "print available commands";
	CmdRet cmdHelp(vector<string> args);
	const string descStatus = "request basic status information from server";
	CmdRet cmdStatus(vector<string> args);
	const string descExtendedstatus = "request detailed status information from server about running transfers";
	CmdRet cmdExtendedstatus(vector<string> args);
	const string descDisconnect = "disconnect from server";
	CmdRet cmdDisconnect(vector<string> args);
	const string descPut = "upload file to server";
	CmdRet cmdPut(vector<string> args);
	const string descGet = "retrieve file from server";
	CmdRet cmdGet(vector<string> args);
	const string descList = "list names of files available on server";
	CmdRet cmdList(vector<string> args);
	const string descExtendedlist = "list files available on server with further information";
	CmdRet cmdExtendedlist(vector<string> args);
	const string descHead = "request the first few bytes of a file from the server";
	CmdRet cmdHead(vector<string> args);
	const string descDeletefile = "delete a file from the server";
	CmdRet cmdDeletefile(vector<string> args);
	const string descKeyfile = "set keyfile to use for encryption";
	CmdRet cmdKeyfile(vector<string> args);
	const string descClosekey = "stop using the previously selected keyfile";
	CmdRet cmdClosekey(vector<string> args);
	const string descNotifications = "request notifications from the server";
	CmdRet cmdNotifications(vector<string> args);
	const string descConnect = "connect to server";
	CmdRet cmdConnect(vector<string> args);
	const string descExit = "exit the application";
	CmdRet cmdExit(vector<string> args);

	const string descLogin = "login to the server";
	CmdRet cmdLogin(vector<string> args);
	const string descSignup = "sign up and login to the server";
	CmdRet cmdSignup(vector<string> args);
	const string descDeleteme = "delete the user you are currently logged in as (needs to be confirmed with the password)";
	CmdRet cmdDeleteme(vector<string> args);
	const string descQueue = "add a file that is already on the server to the queue for sending with the covert channel";
	CmdRet cmdQueue(vector<string> args);
	const string descDequeue = "remove a file from the queue for sending with the covert channel";
	CmdRet cmdDequeue(vector<string> args);

	/**
	 * Method prototypes for commands used internally
	 */
	/* internal execute commands */
	CmdRet cmdVersion(vector<string> args);
	CmdRet cmdPutdata(vector<string> args);
	CmdRet cmdGetdata(vector<string> args);
	CmdRet cmdListdata(vector<string> args);
	CmdRet cmdExtendedlistdata(vector<string> args);

	/**
	 * Method prototypes for handling json responses
	 */
	/* handle commands go here */
	CmdRet handleStatus(Json::Value);
	CmdRet handleExtendedstatus(Json::Value);
	CmdRet handleClose(Json::Value);
	CmdRet handlePut(Json::Value);
	CmdRet handleGet(Json::Value);
	CmdRet handleList(Json::Value);
	CmdRet handleExtendedlist(Json::Value);
	CmdRet handlePutdata(Json::Value);
	CmdRet handleGetdata(Json::Value);
	CmdRet handleListdata(Json::Value);
	CmdRet handleExtendedlistdata(Json::Value);
	CmdRet handleVersion(Json::Value);
	CmdRet handleLogin(Json::Value);
	CmdRet handleSignup(Json::Value);
	CmdRet handleHead(Json::Value);
	CmdRet handleDeletefile(Json::Value);
	CmdRet handleDeleteme(Json::Value);
	CmdRet handleQueue(Json::Value);
	CmdRet handleDequeue(Json::Value);
	CmdRet handleNotifications(Json::Value);
};

#endif
