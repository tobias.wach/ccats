#ifndef JSONHANDLER_H
#define JSONHANDLER_H

#include "qmlhandler.h"
#include <json/json.h>

/**
 * Namespace which handles JSON strings
 */
namespace JsonHandler {
/**
 * Set the qml handler pointer
 * @param q The reference to the qml handler
 */
void setQmlHandler(QMLHandler *q);
/**
 * Parse a JSON string, put them in the terminal and in the log and handle the command using the CmdManager class
 * @param buffer The JSON string
 */
void parseJSON(std::string buffer);
} // namespace JsonHandler

#endif // JSONHANDLER_H
