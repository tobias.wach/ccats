#ifndef CLIMANAGER_H
#define CLIMANAGER_H

#include "config.h"
#include "qmlhandler.h"
#include <QGuiApplication>

/**
 * Namespace which handles the CLI interaction
 */
namespace CliManager {
/**
 * Set the qml handler pointer
 * @param q The reference to the qml handler
 */
void setQmlHandler(QMLHandler *q);
/**
 * Boolean if the user is logged in to enable or disable the background status and list requests
 */
extern bool loggedin;
/**
 * Tries to start the CLI in a new thread
 * @param useSSL Should the CLI be started with the SSL parameter?
 */
void init(bool useSSL);
/**
 * Writes a command to the CLI
 * @param s The command
 */
void writeToCli(QString s);
/**
 * The background loop that reads the output of the CLI and handles it using the jsonhandler
 */
void readPipeLoop();
/**
 * The background loop that requests the notifications and extendedlist using the CLI every 3 seconds
 */
void notificationsLoop();
/**
 * The background loop that requests the status and extendedstatus using the CLI every second
 */
void statusLoop();
/**
 * Exit from the CLI and stop the background threads
 */
void onExit();
/**
 * Set the programActive variable
 * @param active The new value
 */
void setProgramActive(bool active);
} // namespace CliManager

#endif // CLIMANAGER_H
