import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Popup {
    id: confirmDeletePopup
    height: 150
    dim: true
    clip: false
    width: 400
    modal: true
    focus: true
    closePolicy: Popup.NoAutoClose
    anchors.centerIn: Overlay.overlay

    Overlay.modal: Rectangle {
        color: "#b5c5c5c5"
    }

    ColumnLayout {
        anchors.fill: parent

        Text {
            id: confirmDeletePopupConfirmationText
            Layout.alignment: Qt.AlignCenter
            Layout.preferredHeight: 80
            Layout.preferredWidth: parent.width
            text: "Are you sure you want to delete this file from the Server?"
            verticalAlignment: Text.AlignTop
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
            font.pointSize: 15
        }
        RowLayout {
            Layout.preferredHeight: 80
            Layout.preferredWidth: parent.width
            Button {
                id: confirmDeleteButton
                Layout.alignment: Qt.AlignCenter
                Layout.preferredWidth: 100
                text: qsTr("Delete")
                // @disable-check M223
                onClicked: {
                    // @disable-check M222
                    _qmlHandler.onServerFilesConfirmDeleteFileButton(
                                fileNameText)
                }
            }

            Button {
                id: cancelDeleteButton
                Layout.alignment: Qt.AlignCenter
                Layout.preferredWidth: 100
                text: qsTr("Cancel")
                // @disable-check M222
                onClicked: confirmDeletePopup.close()
            }
        }
    }
}
