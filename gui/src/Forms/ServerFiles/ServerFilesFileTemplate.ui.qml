import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Item {
    width: 1250
    height: 50
    property string fileNameText: "Name Placeholder"
    property string fileSizeText: "Size Placeholder"
    property string fileProgressText: "Progress Placeholder"
    property string fileDecryptableText: "Decryptable Placeholder"
    property bool fileExists: false
    property bool fileQueued: false

    Connections {
        target: _qmlHandler
        onServerFilesDisableDownloadButton: {
            if (fileNameText == fileName) {
                fileExists = true
            }
        }

        onServerFilesUpdateFile: {
            if (fileNameText == fileName) {
                fileProgressText = fileProgress
                fileQueued = isQueued
            }
        }

        onServerFilesCloseConfirmDeletePopup: {
            confirmDeletePopup.close()
        }
    }

    RowLayout {
        id: rowLayout
        anchors.fill: parent

        Text {
            id: fileTemplateFileName
            Layout.alignment: Qt.AlignCenter
            Layout.preferredHeight: parent.height
            Layout.preferredWidth: 400
            verticalAlignment: Text.AlignVCenter
            text: fileNameText
        }

        Text {
            id: fileTemplateFileSize
            Layout.alignment: Qt.AlignCenter
            Layout.preferredHeight: parent.height
            Layout.preferredWidth: 100
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            text: fileSizeText
        }

        Text {
            id: fileTemplateFileProgress
            Layout.alignment: Qt.AlignCenter
            Layout.preferredHeight: parent.height
            Layout.preferredWidth: 100
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: fileProgressText
        }

        Text {
            id: fileTemplateFileDecryptable
            Layout.alignment: Qt.AlignCenter
            Layout.preferredHeight: parent.height
            Layout.preferredWidth: 150
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: fileDecryptableText
            color: (fileDecryptableText
                    == "decryptable") ? "#00ad11" : (fileDecryptableText
                                                     == "undecryptable") ? "#df3f3f" : "#000000"
        }

        Button {
            id: fileTemplateQueueButton
            Layout.alignment: Qt.AlignCenter
            Layout.preferredHeight: parent.height
            Layout.preferredWidth: 100
            text: fileQueued ? qsTr("Dequeue") : qsTr("Enqueue")

            // @disable-check M223
            onClicked: {
                // @disable-check M222
                fileQueued ? _qmlHandler.onReceivingDequeueFileButton(
                                 // @disable-check M222
                                 fileNameText) : _qmlHandler.onReceivingQueueFileButton(
                                 fileNameText)
            }
        }

        Button {
            id: fileTemplateDownloadButton
            Layout.alignment: Qt.AlignCenter
            Layout.preferredHeight: parent.height
            Layout.preferredWidth: 200
            enabled: !fileExists
            text: fileExists ? qsTr("Already Downloaded") : qsTr("Download")

            // @disable-check M223
            onClicked: {
                // @disable-check M222
                _qmlHandler.onServerFilesDownloadFileButton(fileNameText)
            }
        }

        Button {
            id: fileTemplateDeleteButton
            Layout.alignment: Qt.AlignCenter
            Layout.preferredHeight: parent.height
            Layout.preferredWidth: 200
            text: qsTr("Delete from server")

            // @disable-check M223
            onClicked: {
                // @disable-check M222
                confirmDeletePopup.open()
            }
        }
    }

    ServerFilesFileTemplateDeletePopup {
        id: confirmDeletePopup
    }
}
