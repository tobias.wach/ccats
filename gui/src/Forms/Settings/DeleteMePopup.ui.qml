import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Popup {
    id: popup
    height: 300
    dim: true
    clip: false
    width: 400
    modal: true
    focus: true
    closePolicy: Popup.NoAutoClose
    anchors.centerIn: Overlay.overlay

    signal resetStatus

    Overlay.modal: Rectangle {
        color: "#b5c5c5c5"
    }

    onClosed: {
        resetStatus()
    }

    Connections {
        target: _qmlHandler
        onDeleteMePopupSetStatus: {
            deleteMePopupStatusText.text = status
        }
    }

    ColumnLayout {
        anchors.fill: parent

        Text {
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 100

            text: qsTr("Are you sure you want to delete your account on the server?\nThe application will restart.")
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 20
        }

        Text {
            id: deleteMePopupStatusText
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 50
            color: "#df3f3f"
            text: ""
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 20
            Connections {
                target: popup
                onResetStatus: deleteMePopupStatusText.text = ""
            }
        }

        TextField {
            id: deleteMePopupPasswordInput
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: 300
            Layout.preferredHeight: 50
            selectByMouse: true
            echoMode: TextInput.Password
            text: ""
            horizontalAlignment: Text.AlignHCenter
            placeholderText: "Enter password to confirm"
            Connections {
                target: popup
                onResetStatus: deleteMePopupPasswordInput.text = ""
            }
        }

        RowLayout {
            spacing: 0
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 50

            Button {
                Layout.alignment: Qt.AlignCenter
                Layout.preferredHeight: parent.height
                Layout.preferredWidth: 150
                text: "Confirm"
                enabled: !(deleteMePopupPasswordInput.text == "")
                font.pointSize: 16
                // @disable-check M223
                onClicked: {
                    // @disable-check M222
                    _qmlHandler.onSettingsDeleteMeButton(
                                deleteMePopupPasswordInput.text)
                }
            }

            Button {
                Layout.alignment: Qt.AlignCenter
                Layout.preferredHeight: parent.height
                Layout.preferredWidth: 150
                text: "Cancel"
                font.pointSize: 16
                // @disable-check M223
                onClicked: {
                    // @disable-check M222
                    popup.close()
                }
            }
        }
    }
}
