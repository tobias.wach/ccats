import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Item {
    width: 1250
    height: 50
    property string notificationDateText
    property string notificationMessageText
    property int myIndex

    RowLayout {
        id: rowLayout
        anchors.fill: parent

        Text {
            id: notificationTemplateDateText
            Layout.alignment: Qt.AlignCenter
            Layout.preferredHeight: parent.height
            Layout.preferredWidth: 200
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHLeft
            text: notificationDateText
        }

        Text {
            id: notificationTemplateMessageText
            Layout.alignment: Qt.AlignCenter
            Layout.preferredHeight: parent.height
            Layout.preferredWidth: 800
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHLeft
            text: notificationMessageText
        }

        Button {
            id: notificationTemplateDismissButton
            Layout.alignment: Qt.AlignCenter
            Layout.preferredHeight: parent.height
            Layout.preferredWidth: 50
            text: qsTr("X")

            // @disable-check M223
            onClicked: {
                // @disable-check M222
                _qmlHandler.onDismissNotificationButton(myIndex)
            }
        }
    }
}
