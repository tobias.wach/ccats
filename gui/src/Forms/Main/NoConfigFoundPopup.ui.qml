import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0

Popup {
    id: popup
    height: 200
    dim: true
    clip: false
    width: 800
    modal: true
    focus: true
    closePolicy: Popup.NoAutoClose
    anchors.centerIn: Overlay.overlay

    Overlay.modal: Rectangle {
        color: "#b5c5c5c5"
    }

    Connections {
        target: _qmlHandler
        onNoConfigFoundPopupClose: {
            popup.close()
        }
        onNoConfigFoundPopupOpen: {
            popup.open()
        }
    }

    ColumnLayout {
        anchors.fill: parent

        Text {
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 50
            id: noConfigFoundPopupText

            text: qsTr("No configuration file found. Default config will be created.\nPlease specify the location of the CLI:")
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 20
        }

        RowLayout {
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 50

            Text {
                Layout.alignment: Qt.AlignCenter
                Layout.preferredHeight: parent.height
                Layout.preferredWidth: 600
                id: noConfigFoundPopupCliPath

                text: qsTr("Select CLI Path >>>")
                horizontalAlignment: Text.AlignLeft
                font.italic: true
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 20
            }

            Button {
                Layout.alignment: Qt.AlignLeft
                Layout.preferredHeight: parent.height
                Layout.preferredWidth: 150
                id: noConfigFoundPopupSelectCliButton
                height: 50
                text: qsTr("Select CLI")
                font.pointSize: 16
                // @disable-check M223
                onClicked: {
                    // @disable-check M222
                    noConfigFoundPopupCliDialog.open()
                }
            }
        }

        Button {
            Layout.alignment: Qt.AlignCenter
            Layout.preferredHeight: 50
            Layout.preferredWidth: 200
            id: noConfigFoundPopupContinueButton
            text: qsTr("Continue")
            enabled: noConfigFoundPopupCliPath.text == "Select CLI Path >>>" ? false : true
            font.pointSize: 16
            // @disable-check M223
            onClicked: {
                // @disable-check M222
                _qmlHandler.onNoConfigFoundPopupContinueButton(
                            noConfigFoundPopupCliPath.text)
                popup.close()
            }
        }
    }

    FileDialog {
        id: noConfigFoundPopupCliDialog
        nameFilters: ["CLI file (ccats-cli)", "Any file (*)"]
        title: "Please select the CLI File"
        folder: shortcuts.home
        // @disable-check M223
        onAccepted: {
            // @disable-check M222
            var path = noConfigFoundPopupCliDialog.fileUrl.toString()
            path = path.replace(/^(file:\/{2})/, "")
            noConfigFoundPopupCliPath.text = path
        }
    }
}
