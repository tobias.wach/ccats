import QtQuick 2.12
import QtQuick.Controls 2.5
import "../ServerFiles"
import "../Settings"
import "../Connect"
import "../Notifications"
import "../Log"

ApplicationWindow {
    id: window
    visible: true
    width: 1280
    height: 720
    maximumHeight: height
    maximumWidth: width
    font.capitalization: Font.MixedCase

    minimumHeight: height
    minimumWidth: width
    title: qsTr("Covert Channel Application")

    property string notificationTabTitle: "Notifications"

    Connections {
        target: _qmlHandler

        onNotification: {
            if (swipeView.currentIndex != 2)
              notificationTabTitle = "* Notifications"
        }
    }

    menuBar: MenuBar {
        contentHeight: 30
        Menu {
            title: qsTr("&File")
            Action {
                text: qsTr("Change Server...")
                onTriggered: {
                    _qmlHandler.onSwitchServer()
                    _qmlHandler.onStart(false)
                    ipPopup.open()
                }
            }
            MenuSeparator { }
            Action {
                text: qsTr("&Quit")
                onTriggered: window.close()
            }
        }
        Menu {
            title: qsTr("&Help")
            Action {
                text: qsTr("About")
                onTriggered: aboutPopup.open()
            }
        }
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: header.currentIndex

        ServerFilesForm {

        }

        NotificationsForm {

        }

        SettingsForm {

        }

        LogForm {

        }
    }

    header: TabBar {
        id: header
        currentIndex: swipeView.currentIndex
        contentHeight: 50

        TabButton {
            text: qsTr("Server Files")
        }

        TabButton {
            text: notificationTabTitle

            onClicked: {
                text = "Notifications"
            }
        }

        TabButton {
            text: qsTr("Settings")
        }

        TabButton {
            text: qsTr("Log")
        }
    }

    footer: FooterForm {

    }

    LoginSignupPopup {
        id: loginSignupPopup
    }

    IpPopup {
        id: ipPopup
    }

    NoConfigFoundPopup {
        id: noConfigFoundPopup
    }

    InvalidConfigPopup {
        id: invalidConfigPopup
    }

    InvalidCliPathPopup {
        id: invalidCliPathPopup
    }

    Popup {
        id: aboutPopup
        height: 300
        dim: true
        clip: false
        width: 400
        modal: true
        focus: true
        anchors.centerIn: Overlay.overlay

        Overlay.modal: Rectangle {
                color: "#b5b5b5e7"
            }

        TextArea {
            anchors.fill: parent
            text: "Covert Channel Application\n\nDeveloped by:\n\nTobias Alexander Wach\nPaul Leonard Sander\nMarius Rescheleit\nDenys Serdyukov\nJonas Pflanzer\n\nLicensed under the GNU General Public License v3.0"
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 20
        }
    }


    Component.onCompleted: {
      swipeView.interactive = false
      ipPopup.open()
      _qmlHandler.onStart(true)
    }
}
