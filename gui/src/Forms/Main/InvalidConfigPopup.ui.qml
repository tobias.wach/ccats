import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0

Popup {
    id: popup
    height: 150
    dim: true
    clip: false
    width: 650
    modal: true
    focus: true
    closePolicy: Popup.NoAutoClose
    anchors.centerIn: Overlay.overlay

    Overlay.modal: Rectangle {
        color: "#b5c5c5c5"
    }

    Connections {
        target: _qmlHandler
        onInvalidConfigPopupClose: {
            popup.close()
        }
        onInvalidConfigPopupOpen: {
            popup.open()
        }
    }

    ColumnLayout {
        anchors.fill: parent

        Text {
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 50
            id: invalidConfigPopupText

            text: qsTr("Configuration file invalid!\nWarning: Creating the default config will overwrite your config file!")
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 20
        }

        RowLayout {
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 50

            Button {
                Layout.alignment: Qt.AlignCenter
                Layout.preferredHeight: parent.height
                Layout.preferredWidth: 200
                id: invalidConfigPopupQuitButton
                text: qsTr("Quit")
                font.pointSize: 16
                // @disable-check M223
                onClicked: {
                    // @disable-check M222
                    _qmlHandler.onInvalidConfigPopupQuitButton()
                }
            }

            Button {
                Layout.alignment: Qt.AlignCenter
                Layout.preferredHeight: parent.height
                Layout.preferredWidth: 200
                id: invalidCliPathPopupContinueButton
                text: qsTr("Create Default")
                font.pointSize: 16
                // @disable-check M223
                onClicked: {
                    // @disable-check M222
                    _qmlHandler.onInvalidConfigPopupCreateDefaultButton()
                    popup.close()
                }
            }
        }
    }
}
