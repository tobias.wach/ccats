import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0

Popup {
    id: popup
    height: 200
    dim: true
    clip: false
    width: 800
    modal: true
    focus: true
    closePolicy: Popup.NoAutoClose
    anchors.centerIn: Overlay.overlay

    Overlay.modal: Rectangle {
        color: "#b5c5c5c5"
    }

    Connections {
        target: _qmlHandler
        onInvalidCliPathPopupClose: {
            popup.close()
        }
        onInvalidCliPathPopupOpen: {
            popup.open()
        }
    }

    ColumnLayout {
        anchors.fill: parent

        Text {
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 50
            id: invalidCliPathPopupText

            text: qsTr("Invalid CLI-Path: CLI could not be found on the location specified in the config file.\nPlease specify the new location of the CLI:")
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 20
        }

        RowLayout {
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 50

            Text {
                Layout.alignment: Qt.AlignCenter
                Layout.preferredHeight: parent.height
                Layout.preferredWidth: 600
                id: invalidCliPathPopupCliPath

                text: qsTr("Select CLI Path >>>")
                horizontalAlignment: Text.AlignLeft
                font.italic: true
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 20
            }

            Button {
                Layout.alignment: Qt.AlignLeft
                Layout.preferredHeight: parent.height
                Layout.preferredWidth: 150
                id: invalicCliPathPopupSelectCliButton
                height: 50
                text: qsTr("Select CLI")
                font.pointSize: 16
                // @disable-check M223
                onClicked: {
                    // @disable-check M222
                    invalidCliPathPopupDialog.open()
                }
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 50

            Button {
                Layout.alignment: Qt.AlignCenter
                Layout.preferredHeight: parent.height
                Layout.preferredWidth: 200
                id: invalidCliPathPopupQuitButton
                text: qsTr("Quit")
                font.pointSize: 16
                // @disable-check M223
                onClicked: {
                    // @disable-check M222
                    _qmlHandler.onInvalidCliPathPopupQuitButton()
                }
            }

            Button {
                Layout.alignment: Qt.AlignCenter
                Layout.preferredHeight: parent.height
                Layout.preferredWidth: 200
                id: invalidCliPathPopupContinueButton
                text: qsTr("Continue")
                enabled: invalidCliPathPopupText.text == "Select CLI Path >>>" ? false : true
                font.pointSize: 16
                // @disable-check M223
                onClicked: {
                    // @disable-check M222
                    _qmlHandler.onInvalidCliPathPopupContinueButton(
                                invalidCliPathPopupCliPath.text)
                    popup.close()
                }
            }
        }
    }

    FileDialog {
        id: invalidCliPathPopupDialog
        nameFilters: ["CLI file (ccats-cli)", "Any file (*)"]
        title: "Please select the CLI File"
        folder: shortcuts.home
        // @disable-check M223
        onAccepted: {
            // @disable-check M222
            var path = invalidCliPathPopupDialog.fileUrl.toString()
            path = path.replace(/^(file:\/{2})/, "")
            invalidCliPathPopupCliPath.text = path
        }
    }
}
