#include <QDebug>

#include "../include/cmdmanager.h"
#include "../include/jsonhandler.h"

using namespace std;

namespace JsonHandler {
QMLHandler *qmlHandler;
}

void JsonHandler::setQmlHandler(QMLHandler *q) { qmlHandler = q; }

// This method gets a string and tries to read it as Json
// If it fails to do so, return and do nothing, else handle the content
void JsonHandler::parseJSON(string buffer) {
	Json::Value root;
	Json::CharReaderBuilder builder;
	Json::CharReader *reader = builder.newCharReader();
	string jsonError;

	// Try to parse the string as Json and store the result of the parsing in a boolean
	bool parsingSuccessful = reader->parse(buffer.c_str(), buffer.c_str() + buffer.size(), &root, &jsonError);

	delete (reader);

	// If the string is not correct Json, return
	if (!parsingSuccessful) {
		return;
	}

	string cmd = root["command"].asString();
	string err = root["error"].asString();

	// Filter the commands to not be printed
	if (cmd.compare("notifications") && cmd.compare("extendedlist") && cmd.compare("extendedstatus") && cmd.compare("status") && cmd.compare("putdata") &&
	    cmd.compare("getdata")) {
		emit qmlHandler->log(QString::fromStdString(buffer));
		qInfo() << QString::fromStdString(buffer);
	}

	if (err.compare("") && cmd.compare("connectionerror")) {
		emit qmlHandler->footerSetError(QString::fromStdString(err));
	}

	CmdManager::executeCmd(cmd, root);
}
