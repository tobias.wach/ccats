#include <QApplication>
#include <QObject>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QQmlContext>
#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <sys/prctl.h>
#include <sys/wait.h>
#include <thread>
#include <unistd.h>

#include "../include/climanager.h"
#include "../include/cmdmanager.h"
#include "../include/jsonhandler.h"
#include "../include/qmlhandler.h"

using namespace std;

int main(int argc, char *argv[]) {
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

	QCoreApplication::setOrganizationName("CCats");

	QApplication app(argc, argv);

	QQmlApplicationEngine engine;

	QMLHandler qmlHandler;

	qmlHandler.setConfigExists(Config::loadFile());
	CmdManager::setQmlHandler(&qmlHandler);
	CmdManager::init();
	CliManager::setQmlHandler(&qmlHandler);
	JsonHandler::setQmlHandler(&qmlHandler);

	// Set the context for the window, so that the qml files can be connected to the qmlHandler
	engine.rootContext()->setContextProperty("_qmlHandler", &qmlHandler);

	// Load the main window
	QQmlComponent component(&engine, QUrl(QStringLiteral("qrc:/Forms/Main/main.qml")));

	QObject *object = component.create();

	// Run the main window and save the return of the application in an integer ret
	// This is blocking, which means it will block the main program until the window is closed
	int ret = app.exec();

	// If we land here, the window has been closed. Properly disconnect from the server now
	CliManager::onExit();

	// Restart this application if the _RESTART variable is set (for example when the user deletes their account)
	if (_RESTART) {
		pid_t pid = 0;
		pid = fork();
		if (pid == 0) {
			execl("./CCats-GUI", "CCats-GUI", (char *)NULL);
			exit(1);
		}
	}

	delete object;

	return ret;
}
