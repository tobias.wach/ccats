#include <QDebug>
#include <QGuiApplication>
#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <poll.h>
#include <string>
#include <sys/prctl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <thread>
#include <unistd.h>

#include "../include/climanager.h"
#include "../include/cmdmanager.h"
#include "../include/config.h"
#include "../include/jsonhandler.h"
#include "../include/qmlhandler.h"
#include <iostream>
#include <json/json.h>

using namespace std;

QUrl sendFileUrl = QUrl("");
bool _RESTART = false;
bool configExists = false;

QMLHandler::QMLHandler(QObject *parent) : QObject(parent) {}

void QMLHandler::loadSettingsToGUI() {
	stringstream ss;

	// The space is needed to get the stringstream to perform proper formatting of multiple values
	bool saveIP, saveUsername;
	ss << Config::getValue("Autofill-IP") << " " << Config::getValue("Autofill-Username");
	ss >> saveIP >> saveUsername;

	QString cliPath = QString::fromStdString(Config::getValue("CLI-Path"));
	QString keyPath = QString::fromStdString(Config::getValue("Keyfile-Path"));
	emit loadSettings(saveIP, saveUsername, cliPath, keyPath);
}

// ### QML Handlers ###

// Main
void QMLHandler::onStart(bool startWithCli) {
	if (configExists) {
		// Config exists
		if (Config::checkConfig()) {
			// Config is valid
			if (Config::getValue("Autofill-IP") == "1") {
				emit ipPopupSetIP(QString::fromStdString(Config::getValue("Default-IP")));
				emit ipPopupCheckSaveCheckbox();
			}
			if (Config::getValue("Autofill-Username") == "1") {
				emit loginSetUsername(QString::fromStdString(Config::getValue("Default-Username")));
				emit loginSignupCheckSaveCheckbox();
			}
			if (!ifstream(Config::getValue("CLI-Path"))) {
				// Invalid CLI Path
				emit invalidCliPathPopupOpen();
			} else if (startWithCli) {
				// CLI exists, check for SSL
				if (Config::getValue("Use-SSL") == "1") {
					if (!ifstream(Config::getValue("SSL-Path"))) {
						// SSL file does not exist
						emit ipPopupSetStatus("Invalid SSL-Path. SSL has been disabled.");
						CliManager::init(false);
					} else {
						// All good, start CLI with SSL
						CliManager::init(true);
					}
				} else {
					// All good, SSL is disabled, start CLI without SSL
					CliManager::init(false);
				}
			}
		} else {
			// Config is invalid
			emit invalidConfigPopupOpen();
		}
	} else {
		// Config doesn't exist
		Config::setupDefaultConfig();
		emit noConfigFoundPopupOpen();
	}
}

void QMLHandler::onSwitchServer() { CliManager::writeToCli("disconnect"); }

// No Config Found Popup
void QMLHandler::onNoConfigFoundPopupContinueButton(QString cli_path) {
	Config::setValue("CLI-Path", cli_path.toUtf8().constData());
	Config::saveFile();
	onStart(true);
}

// Invalid Cli Path Popup
void QMLHandler::onInvalidCliPathPopupContinueButton(QString cli_path) {
	Config::setValue("CLI-Path", cli_path.toUtf8().constData());
	Config::saveFile();
	onStart(true);
}

void QMLHandler::onInvalidCliPathPopupQuitButton() { emit closeWindow(); }

// Invalid Config Popup
void QMLHandler::onInvalidConfigPopupQuitButton() { emit closeWindow(); }

void QMLHandler::onInvalidConfigPopupCreateDefaultButton() {
	Config::setupDefaultConfig();
	emit noConfigFoundPopupOpen();
}

// Server Files
void QMLHandler::onServerFilesSelectFileButton(QUrl url) {
	sendFileUrl = url.toLocalFile();
	emit log("File Selected: " + sendFileUrl.toString());
	emit serverFilesSetFileUrlText(sendFileUrl.toString());
	emit serverFilesEnableSendButton();
}

void QMLHandler::onServerFilesSendFileButton() {
	QString command = "put \"" + sendFileUrl.toString() + "\"";
	CliManager::writeToCli(command);
}

void QMLHandler::onServerFilesClearSelectionButton() {
	sendFileUrl = QUrl("");
	emit log("Cleared Selection");
	emit serverFilesSetFileUrlText("None");
	emit serverFilesDisableSendButton();
}

void QMLHandler::onServerFilesDownloadFileButton(QString fileName) {
	QString command = "get \"" + fileName + "\"";
	CliManager::writeToCli(command);
}

void QMLHandler::onServerFilesConfirmDeleteFileButton(QString fileName) {
	QString command = "deletefile \"" + fileName + "\"";
	CliManager::writeToCli(command);
}

// Settings
void QMLHandler::onKeyfileSelected(QString path) {
	QString command = "keyfile \"" + path + "\"";
	CliManager::writeToCli(command);
}
void QMLHandler::onKeyfileClosed() { CliManager::writeToCli("closekey"); }

void QMLHandler::onSettingsDeleteMeButton(QString password) {
	QString command = "deleteme " + password;
	CliManager::writeToCli(command);
}

void QMLHandler::onSettingsRevertChangesButton() {
	Config::loadFile();
	loadSettingsToGUI();
	emit log("Settings changes reverted.");
}

void QMLHandler::onSettingsResetButton() {
	string cli_path = Config::getValue("CLI-Path");
	Config::setupDefaultConfig();
	Config::setValue("CLI-Path", cli_path);
	loadSettingsToGUI();
	emit log("Settings resetted to default.");
}

void QMLHandler::onSettingsSaveButton(bool saveIP, bool saveUsername, QString cliPath, QString keyPath) {
	Config::setValue("Autofill-IP", to_string(saveIP));
	Config::setValue("Autofill-Username", to_string(saveUsername));
	Config::setValue("CLI-Path", cliPath.toUtf8().constData());
	Config::setValue("Keyfile-Path", keyPath.toUtf8().constData());
	Config::saveFile();
	emit log("Settings saved.");
}

// Ip Popup
void QMLHandler::onIpPopupConnectButton(QString ip, bool saveAsDefault) {
	QStringList ipport = ip.split(":");
	QString command = "connect " + ipport[0];

	CmdManager::setCachedIP(ipport[0]);
	if (ipport.size() > 1) {
		command += " " + ipport[1];
		CmdManager::setCachedPort(ipport[1]);
	}

	CliManager::writeToCli(command);

	emit ipPopupDisableConnectButton();
	if (saveAsDefault) {
		Config::setValue("Default-IP", ip.toUtf8().constData());
		Config::setValue("Autofill-IP", "1");
		Config::saveFile();
		loadSettingsToGUI();
	}
}

// Login
void QMLHandler::onLoginLoginButton(QString username, QString password, bool saveAsDefault) {
	QString command = "login " + username + " " + password;
	CliManager::writeToCli(command);
	emit loginDisableLoginButton();
	if (saveAsDefault) {
		Config::setValue("Default-Username", username.toUtf8().constData());
		Config::setValue("Autofill-Username", "1");
		Config::saveFile();
		loadSettingsToGUI();
	}
}

// Signup
void QMLHandler::onSignupRegisterButton(QString username, QString passwordOne, QString passwordTwo, bool saveAsDefault) {
	if (QString::compare(passwordOne, passwordTwo, Qt::CaseSensitive)) {
		emit signupSetStatus("Passwords don't match");
		return;
	}
	QString command = "signup " + username + " " + passwordOne;
	CliManager::writeToCli(command);
	emit signupDisableRegisterButton();
	if (saveAsDefault) {
		Config::setValue("Default-Username", username.toUtf8().constData());
		Config::setValue("Autofill-Username", "1");
		Config::saveFile();
		loadSettingsToGUI();
	}
}

// Notifications
void QMLHandler::onDismissNotificationButton(int index) { emit dismissNotification(index); }

// Queueing
void QMLHandler::onReceivingQueueFileButton(QString fileName) {
	QString command = "queue " + fileName;
	CliManager::writeToCli(command);
}

void QMLHandler::onReceivingDequeueFileButton(QString fileName) {
	QString command = "dequeue " + fileName;
	CliManager::writeToCli(command);
}

void QMLHandler::setRestart(bool restart) { _RESTART = restart; }

void QMLHandler::setConfigExists(bool exists) { configExists = exists; }
