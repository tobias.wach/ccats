# Using the Graphical User Interface (GUI)

The GUI of the Covert Channel Application provides an easy way to interact with a covert channel server. It is using the CLI for this interaction, so a valid CLI executable is required for it to work.


## Starting the GUI

The graphical user interface should be started without any additional parameters.


## Configuration

The GUI uses a configuration file. This file should be called <i>configGUI.txt</i> and should be located in the same folder where the GUI is executed from. On start, the program will check if a valid configuration file exists and if the specified CLI path is valid.

If no configuration file exists, the program will generate a default file. The user will be notified about this and will be asked to set a path to the CLI.

If the configuration file is not valid, the user will get notified and has the option to either quit the program or to generate the default configuration file. The latter would overwrite the existing one.

Any changes made to the configuration using the GUI have to be explicitly saved using the "Save Changes" button. This includes the selection of a keyfile.

### Configuration Values

`Autofill-IP`: Should the default IP automatically be filled in on startup.<br/>
`Autofill-Username`: Should the default Username automatically be filled in on startup.<br/>
`CLI-Path`: The absolute path to the command line interface (CLI) file.<br/>
`Default-IP`: The default IP to be used for the autofill.<br/>
`Default-Username`: The default Username to be used for the autofill.<br/>
`Keyfile-Path`: The absolute path to the keyfile to be used for encryption.<br/>
`Use-SSL`: Should the SSL file be used for a secure connection.<br/>
`SSL-Path`: The absolute path to the SSL file to be used.<br/>


## Connecting to a server

On startup, the GUI asks the user for an IP-address to connect to. This address can contain a port (for example `127.0.0.1:1234`). If no port is specified, the default port <i>1234</i> is used. Additionally, the user has the option to set the given IP-address as the default one in the configuration file.

If the connection was successful, the user will be asked to either log in or sign up using the given forms. Additionally, the user has the option to set the given username as the default one in the configuration file.


## Layout Overview

The GUI is splitted into 3 parts: The menubar, the main window and the footer.

The menubar on the top gives the user the option to change the server, to quit the application and to show the <i>about</i> page.

The footer shows the user some information about his active connection such as the server status, the IP-address and their username. Additionally, any error that occurs will be shown here for 3 seconds.

The main window contains of the following 4 tabs:

### Server Files

This tab shows the user all the files that are currently on the server, including their size, encryption and status. The user has the option to enqueue/dequeue, download or delete the files.

Downloading a file will download it to the same folder where the GUI is executed from.

At the bottom, the user can select a file to be uploaded to the server. The selection is done with a file dialog.

### Notifications

This tab shows the user all notifications that get sent from the server (for example when a transfer is complete). Every notification can be dismissed. Additionally, notifications will trigger a system notification.

The title of this tab will contain a star (<b>* Notifications</b>) when there is a new notification.

### Settings

This tab shows the user the current settings that are specified in the configuration file. These can be changed and saved to the file. Additionally the user has the option to reset the settings to the default values.

In this tab, the user can also delete his account from the server. This requires the users password.

### Log

This tab shows the user the log.
